## Build environment ##
ARG node_version=10.7
FROM node:${node_version}-stretch as stage

# Install Chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable

# Set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# Add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# Install and cache app dependencies
COPY package.json /usr/src/app/package.json
RUN npm install
RUN npm install -g @angular/cli

# Add app
COPY . /usr/src/app

# Run tests
RUN ng test --watch=false

# Generate build
RUN npm run build

## Production ##
FROM nginx:1.15-alpine

# Copy artifact build from the build environment
#RUN rm -rf /usr/share/nginx/html/*
COPY --from=stage /usr/src/app/dist /usr/share/nginx/html

# Expose port 80
EXPOSE 80

# Run Nginx
CMD ["nginx", "-g", "daemon off;"]
